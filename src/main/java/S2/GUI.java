package S2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI {
    public JPanel jPanel;
    private JTextField textField1;
    private JButton button1;
    private JTextField textField2;

    public GUI() {
        textField2.setEditable(false);
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String inputText = textField1.getText();
                textField2.setText(inputText);
            }
        });
    }
}

class Main{
    public static void main(String[] args) {
        JFrame jFrameNew = new JFrame("");
        jFrameNew.setContentPane(new GUI().jPanel);
        jFrameNew.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrameNew.pack();
        jFrameNew.setLocationRelativeTo(null);
        jFrameNew.setVisible(true);
    }
}